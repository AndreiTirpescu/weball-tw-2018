window.onload = function() 
{
    var loaderExpandables = new ClickLoader('button-expand', expandTournament)
    var loaderJSONGenerators = new ClickLoader('json-generator', loadJson)
    var loaderXMLGenerators = new ClickLoader('xml-generator', loadXml)
    var loaderCSVGenerators = new ClickLoader('csv-generator', loadCSV)

    loaderExpandables.load()
    loaderJSONGenerators.load()
    loaderXMLGenerators.load()
    loaderCSVGenerators.load()
}

function expandTournament() 
{
    this.className += ' tournament-expanded';
    this.onclick = unexpandTournament;

    var childNodes = this.children;
    for(var i in childNodes)
    {
        try
        {
            if(childNodes[i].className === 'wrapper-features-hidden')
            {
                childNodes[i].className = 'wrapper-features'
            }

        } 
        catch(e) 
        {}
    }
}

function unexpandTournament() 
{
    var str = this.className.replace(' tournament-expanded', '')
    this.className = str;
    this.onclick = expandTournament;
    
    var childNodes = this.children;
    for(var i in childNodes)
    {
        try
        {
            if(childNodes[i].className === 'wrapper-features')
            {
                childNodes[i].className = 'wrapper-features-hidden'
            }

        } 
        catch(e) 
        {}
    }
}

function loadJson() 
{
    var dummyTournament = new Tournament("dummyName", "dummyType", "dummyState")
    console.info(dummyTournament.generateJSON())
}

function loadXml()
 {
    var dummyTournament = new Tournament("dummyName", "dummyType", "dummyState")
    console.info(dummyTournament.generateXML())
}


function loadCSV() 
{
    var dummyTournament = new Tournament("dummyName", "dummyType", "dummyState")
    console.info(dummyTournament.generateCSV())
}


class Tournament 
{
    constructor(name, type, state) 
    {
        this.tournamentName  = name
        this.tournamentType  = type
        this.tournamentState = state
    }

    tournamentName() 
    {
        return this.tournamentName
    }

    tournamentState() 
    {
        return this.tournamentState
    }

    tournamentType()
    {
        return this.tournamentType
    }

    static XML_HEADER()
    {
        return '<?xml version="1.0" encoding="UTF-8"?>\n'
    }

    static CSV_HEADER()
    {
        return 'name;type;state\n'
    }

    generateJSON()
    {        
        var jsonStr = JSON.stringify({name:  this.tournamentName, 
                        type:  this.tournamentType,
                        state: this.tournamentState,
                        other: "other" });

        var dynamicFileSaver = new DynamicFileSaver(jsonStr, {type : 'application/json'}, 
                                                    "dummy-tournament.json")
        dynamicFileSaver.buildFile()
        dynamicFileSaver.downloadFile()
    }



    generateXML()
    {
        var xmlStr = Tournament.XML_HEADER();
        xmlStr += '<tournament>\n';
        xmlStr += '<name>' + this.tournamentName + '</name>\n';
        xmlStr += '<type>' + this.tournamentType + '</type>\n';
        xmlStr += '<state>' + this.tournamentState + '</state>\n';
        xmlStr += '</tournament>';

        var dynamicFileSaver = new DynamicFileSaver(xmlStr, {type : 'application/xml'}, 
                                                    "dummy-tournament.xml")
        dynamicFileSaver.buildFile()
        dynamicFileSaver.downloadFile()
    }


    generateCSV()
    {
        var csvStr = Tournament.CSV_HEADER();
        csvStr += (this.tournamentName + ';' + this.tournamentType + ';' + this.tournamentState + ';\n');

        var dynamicFileSaver = new DynamicFileSaver(csvStr, {type : 'application/csv'},  "dummy-tournament.csv");

        dynamicFileSaver.buildFile();
        dynamicFileSaver.downloadFile();
    }

    generatePDF()
    {
        return "PDF DOCUMENT"
    }
}



class ClickLoader 
{
    constructor(className, methodCallback)
    {
        this.className = className;
        this.methodCallback = methodCallback;
    }

    load()
    {
        var array = document.getElementsByClassName(this.className)
        for(var i in array)
        {
            console.info(array[i])
            // baguiala de limbaj de programare soto
            try {
                array[i].onclick = this.methodCallback;
            } catch(e)
            {
                console.info(e)
            }
        }
    }
}

class DynamicFileSaver
{
    constructor(fileContent, fileType, fileName)
    {
        this.fileContent = fileContent;
        this.fileType    = fileType;
        this.fileName    = fileName;
    }

    fileContent() 
    {
        return this.fileContent;
    }

    fileType()
    {
        return this.fileType
    }

    setFileContent(fileContent)
    {
        this.fileContent = fileContent;
    }

    setFileType(fileType)
    {
        this.fileType    = fileType;
    }

    buildFile()
    {
        this.file = new Blob([this.fileContent], this.fileType)
        this.url  = window.URL.createObjectURL(this.file)
    }

    downloadFile()
    {
        var linkElement = document.createElement("a")
        
        linkElement.href = this.url
        linkElement.download = this.fileName
        linkElement.style.display = 'none'

        document.body.appendChild(linkElement)
        linkElement.click()
        document.body.removeChild(linkElement)
    }
};