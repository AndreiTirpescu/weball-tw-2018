<?php
    if(isset($_GET['url']))
    {
        if( substr($_GET['url'], 0, 8) == "api/rest" && strstr(getallheaders()['Accept'], 'text/html') == "")
        {
            require_once("requesthandlefactory.php");
            require_once("requesthandle.php");
            $reqHandleFactory = new RequestHandleFactory($_SERVER["REQUEST_METHOD"]);
            $reqHandle = $reqHandleFactory->getCurrentHandle();
            $reqHandle->comunicate();
        }
    }
?>