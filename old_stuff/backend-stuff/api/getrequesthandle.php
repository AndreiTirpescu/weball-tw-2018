<?php 

require_once("requesthandle.php");
require_once("apicallfactoryget.php");

class GetRequestHandle extends RequestHandle
{
    protected function handleReceive()
    {
        $apiCallStr = $_GET['url'];
        $apiCallStr = str_replace("api/rest/", "", $apiCallStr);
        
        $apiCallFactory = new ApiCallFactoryGet($apiCallStr);
        $this->apiCall  = $apiCallFactory->apiCall();
        $this->apiCall->call();
    }

    protected function sendResponse()
    {
        header('Content-Type: "application/json"');
        $this->apiCall->response();
    }
}
?>

