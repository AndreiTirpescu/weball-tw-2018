<?php 

require_once("requesthandle.php");

class UndefinedRequestHandle extends RequestHandle
{
    protected function handleReceive()
    {
    }

    protected function sendResponse()
    {
        echo "400 Bad Request";
    }
}

?>