<?php

interface ApiCall
{
    public function call();
    public function response();
}

?>
