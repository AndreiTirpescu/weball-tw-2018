<?php

require_once("requesthandle.php");
require_once("getrequesthandle.php");
require_once("postrequesthandle.php");
require_once("undefinedrequesthandle.php");

class RequestHandleFactory 
{
    private $currentHandle;

    function __construct($verb) 
    {
        switch($verb)
        {
            case "GET":
                $this->currentHandle = new GetRequestHandle;
            break;

            case "POST":
                $this->currentHandle = new PostRequestHandle;
            break;

            default:
                $this->currentHandle = new UndefinedRequestHandle;
            break;

        }
    }

    public function getCurrentHandle()
    {
        return $this->currentHandle;
    }
}


?>