<?php
require_once('route.php');
require_once('controller/abstractcontroller.php');

class IndexRoute extends Route
{
    function __construct()
    {
        if(!isset($_GET['url']))
        {
            $this->load();
        }
    }

    public function load()
    {    
        $var = new Controller("view/indexview.php");
        $var->load();
    }
}

?>