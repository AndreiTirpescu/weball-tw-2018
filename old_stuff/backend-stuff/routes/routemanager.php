<?php
require_once('route.php');
require_once('aboutusroute.php');
require_once('indexroute.php');

class RouteManager 
{
    private $routesArray = array();

    function __construct()
    {
        $this->initAllRoutes();
    }

    public function initAllRoutes()
    {
        array_push($this->routesArray, new AboutUsRoute('about-us'));
        array_push($this->routesArray, new IndexRoute);
    }
}

?>