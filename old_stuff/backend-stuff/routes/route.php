<?php

abstract class Route 
{
    protected $routePath;

    public function __construct($path)
    {
        $this->routePath = $path;
        
        if(isset($_GET['url']) && substr($_GET['url'], 0, strlen($this->routePath)) == $this->routePath)
        {
            $this->load();
        }
    }
    abstract public function load();
}

?>