<?php
class Controller
{
    private $viewPath;

    function __construct($path)
    {
        $this->viewPath = $path;
    }

    public function load()
    {
        require_once($this->viewPath);
    }
}
?>