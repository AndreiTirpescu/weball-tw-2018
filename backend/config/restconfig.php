<?php

final class RestConfig 
{
    static private $configPath = "config.ini";
    private $configMap;

    private function __construct() 
    {
        $this->loadConfig();
    }

    static public  function instance() 
    {
        static $instance = null;
        if($instance == null)
        {
            $instance = new RestConfig();
        }
        return $instance;
    }

    public function loadConfig()
    {
        $this->configMap = parse_ini_file(self::$configPath, true, INI_SCANNER_TYPED);
        echo var_dump($this->configMap);
    }

    public function shouldRemoveDebugMessageBeforeSend()
    {
        return $this->configMap["DEBUG"]["clear_before_send"];
    }

}

?>