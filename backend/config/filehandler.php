<?php


final class FileHandler 
{
    private $file;
    private $fileName = "";

    public function __construct() 
    {}

    public function createFile($fileName)
    {
        $this->file = fopen($fileName, "w");
        $this->fileName = $fileName;
    }

    public function writeToFile($data)
    {
        fwrite($this->file, $data);
    }

    public function closeFile()
    {
        fclose($this->file);
    }

    public function send($type)
    {
        header('Content-Type: '.$type.'; charset=utf-8');
        header('Content-Disposition: attachment; filename="'.$this->fileName.'"');
        echo file_get_contents($this->fileName);
    }
}


?>