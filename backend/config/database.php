<?php

final class DatabaseConnection 
{
    private $db_host = "localhost";
    private $db_user = "root";
    private $db_pass = "";
    private $db_name = "weballdb";
    private $db_connection = null;

    private $last_id;

    private function __construct() 
    {
       $this->connect();
    }

    private function connect() 
    {
        $dsn = "mysql:host=".$this->db_host.";dbname=".$this->db_name;
        $options = array();
        $options[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES utf8';
        $options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        try 
        {
            $this->db_connection = new PDO ($dsn, $this->db_user, $this->db_pass, $options);
        } 
        catch (PDOException $e) 
        { 
            echo "Could not connect to db".PHP_EOL;
            echo var_dump($e).PHP_EOL;
            return;
        } 
        
        echo "Succesfuly connected to database".PHP_EOL;
        $this->db_connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    static public  function instance() 
    {
        static $instance = null;
        if($instance == null)
        {
            $instance = new DatabaseConnection();
        }
        return $instance;
    }

    public function query($query, $assocArray = []) 
    {
        $statement = $this->db_connection->prepare($query);
        $statement->execute($assocArray);
        return $statement->fetchAll();
    }

    public function insert($query, $assocArray = []) 
    {
        $statement = $this->db_connection->prepare($query);
        return  $statement->execute($assocArray);
    }

    //use with care
    public function lastID($tName, $idName)
    {
        $query = 'SELECT ' .$idName. ' FROM '.$tName.' ORDER BY '. $idName .' DESC LIMIT 1';
        $statement = $this->db_connection->prepare($query);
        $statement->execute([]);
        return $statement->fetchAll();
    }
}

?>