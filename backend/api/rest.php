<?php
    require_once("config/database.php");
    
    if(isset($_GET['url']))
    {
        if( substr($_GET['url'], 0, 8) == "api/rest")
        {
            require_once("requesthandlefactory.php");
            require_once("requesthandle.php");

            $reqHandleFactory = new RequestHandleFactory($_SERVER["REQUEST_METHOD"]);
            $reqHandle = $reqHandleFactory->getCurrentHandle();
            $reqHandle->comunicate();    
            
            
        }
    }
?>