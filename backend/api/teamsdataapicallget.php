<?php

require_once("apicall.php");

class GetTeamsDataApiCall extends ApiCall
{
    private $error = "";

    private $json_result = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $query = 'SELECT teamID, name FROM team WHERE UPPER(sport) = UPPER(:sport) ';

        $assocArray = [ 'sport' => $_GET["sport"]];

        $result = DatabaseConnection::instance()->query($query, $assocArray);
        
        if(count($result) > 0)
        {
            $this->json_result = json_encode($result);
            echo var_dump($this->json_result);
        }
        else
        {
            $this->error = "Tournament doesn't exist in database";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            // $arr['new_user'] = '';
            $arr['data'] = $this->json_result;
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        
        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}

?>