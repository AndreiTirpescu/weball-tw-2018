<?php
require_once("apicall.php");

class TournamentInfoApiCall extends ApiCall
{
    private $error = "";
    private $tournament_entry_json;
    private $tournament_info_json;
    private $tournament_events_json;

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
        }

        $query = 'SELECT * FROM tournament t1 join tournament_entry t2 on t1.tournamentID = t2.tournament_id JOIN team t3 on t3.teamID = t2.team_id WHERE t1.tournamentID = :id';
        $assocArray = ['id' => $_GET['id']];
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        $this->tournament_entry_json = json_encode($result);

        $query = 'SELECT * FROM tournament WHERE tournamentID = :id';
        $assocArray = ['id' => $_GET['id']];
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        $this->tournament_info_json = json_encode($result);

        
        $query = 'SELECT * FROM event WHERE tournamentID = :id';
        $assocArray = ['id' => $_GET['id']];
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        $this->tournament_events_json = json_encode($result);
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            $arr['entries'] = $this->tournament_entry_json;
            $arr['tournament'] =  $this->tournament_info_json;
            $arr['events'] = $this->tournament_events_json;
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}
?>