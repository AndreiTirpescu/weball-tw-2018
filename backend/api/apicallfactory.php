<?php
require_once("apicall.php");
require_once("undefinedapicall.php");
require_once("loginapicall.php");
require_once("signupapicall.php");

class ApiCallFactory
{
    private $apiCall;
    
    function __construct($apiCallName)
    {
        // echo $apiCallName;

        switch($apiCallName)
        {
            case "user.login":
                $this->apiCall = new LoginApiCall;
            break;

            case "user.signup":
                $this->apiCall = new SignUpApiCall;
            break;
            
            default:
                $this->apiCall = new UndefinedApiCall;
            break;
        }
    }

    function apiCall()
    {
        return $this->apiCall;
    }
}
?>