<?php

require_once("apicall.php");

class TournamentEntryApiCall extends ApiCall
{
    private $error = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);

        $query = "INSERT INTO tournament_entry(team_ID, tournament_ID) VALUES(:teamid, :tournamentid)";
        $assocArray = [ 'teamid' => $json_map["teamid"], 'tournamentid' => $json_map["tournamentid"] ];

        if( DatabaseConnection::instance()->insert($query, $assocArray) === false )
        {
            $this->error = "Could not register team to tournament";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            // $arr['new_user'] = '';
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}


?>