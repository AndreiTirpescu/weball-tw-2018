<?php

require_once("apicall.php");

class CommentApiCall extends ApiCall
{
    private $error = "";

    private $json_result = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);
    
        $assocArray = array();

        $insert = "INSERT INTO comment(userID ,commentString, tournamentID) VALUES(:id, :commentString, :tournamentID)";
        echo $insert;

        $assocArray = [ 'id' => $this->id_for_token, 'commentString' => $json_map["commentString"], 'tournamentID' => $json_map["tournamentID"] ];
    
        if(  DatabaseConnection::instance()->insert($insert, $assocArray) === false )
        {
            $this->error = "Couldn't insert in comments table";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            // $arr['new_user'] = '';
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}


?>