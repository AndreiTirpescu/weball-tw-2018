<?php
require_once("apicall.php");
require_once("undefinedapicall.php");
require_once("loginapicall.php");
require_once("signupapicall.php");
require_once("config/database.php");
require_once("addtournamentapicall.php");
require_once("updateuserdata.php");
require_once("addteamapicall.php");
require_once("tournamententryapicall.php");
require_once("commentapicall.php");
require_once("userupgradeapicall.php");
require_once("setuptournamentapicall.php");
require_once("updatescoresapicall.php");

class ApiCallFactoryPost
{
    private $apiCall;
    
    function __construct($apiCallName)
    {
        echo $apiCallName;

        switch($apiCallName)
        {
            case "user.login":
                $this->apiCall = new LoginApiCall;
            break;

            case "user.signup":
                $this->apiCall = new SignUpApiCall;
            break;
            
            case "tournament.add":
                $this->apiCall = new AddTournamentApiCall;
            break;

            case "user.update":
                $this->apiCall = new UpdateUserApiCall;
            break;

            case "team.add":
                $this->apiCall = new AddTeamApiCall;
            break;

            case "tournament.entry":
                $this->apiCall = new TournamentEntryApiCall;
            break;

            case "user.comment":
                $this->apiCall = new CommentApiCall;
            break;
            
            case "user.upgrade":
                $this->apiCall = new UserUpgradeApiCall;
            break;

            case "tournament.setup":
                $this->apiCall = new TournamentSetupApiCall;
            break;
            
            case "update.scores":
                $this->apiCall = new UpdateScoresApiCall;
            break;

            default:
                $this->apiCall = new UndefinedApiCall;
            break;
        }
    }

    function apiCall()
    {
        return $this->apiCall;
    }
}
?>