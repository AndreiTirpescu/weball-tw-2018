<?php

require_once("apicall.php");

class UserUpgradeApiCall extends ApiCall
{
    private $error = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        if(!$this->isAdmin())
        {
            $this->error = "User access denied";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);

        $query = 'UPDATE user SET isAdmin=1  WHERE email=:email;';

        for ($i=0; $i<count($json_map["email"]); ++$i)
        {
            $assocArray = ["email" => $json_map["email"][$i]];
            DatabaseConnection::instance()->insert($query, $assocArray);
            echo $query;
        }
        
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        
        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}

?>