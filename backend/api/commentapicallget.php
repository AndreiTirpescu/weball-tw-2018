<?php

require_once("apicall.php");

class CommentApiCallGet extends ApiCall
{
    private $error = "";

    private $json_result = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);
    
        $assocArray = array();

        $select = "SELECT * FROM user u join comment c on c.userID = u.userID WHERE c.tournamentID = :tournamentID";

        echo $select;

        $assocArray = [ 'tournamentID' => $_GET['tournamentID']];
    
        $result = DatabaseConnection::instance()->query($select, $assocArray);
        
        if(count($result) > 0)
        {
            $this->json_result = json_encode($result);
            echo var_dump($this->json_result);
        }
        else
        {
            $this->error = "Tournament doesn't exist in database";
        }

    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
             $arr['data'] = $this->json_result;
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}


?>