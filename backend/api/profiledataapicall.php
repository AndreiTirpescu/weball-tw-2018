<?php
require_once("apicall.php");

class ProfileDataApiCall extends ApiCall
{
    private $error = "";
    private $resp = array();

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $query = 'SELECT * FROM user WHERE userID = :id';

        $assocArray = [ 'id' => $this->id_for_token ];
              
        $result = DatabaseConnection::instance()->query($query, $assocArray);

        if(count($result) != 1)
        {
            $this->error = "User data not found";
            return;
        }

        $this->resp["username"] = $result[0]->name;
        $this->resp["email"] = $result[0]->email;
        $this->resp["profilePic"] = $result[0]->profilePicture;
        $this->resp["isAdmin"] = $result[0]->isAdmin;
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            $arr['data'] = $this->resp;
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}
?>