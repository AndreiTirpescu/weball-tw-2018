<?php
abstract class RequestHandle
{
    protected $apiCall;

    abstract protected function handleReceive();
    abstract protected function sendResponse();

    public function comunicate()
    {
        $this->handleReceive();
        $this->sendResponse();
    }
}
?>