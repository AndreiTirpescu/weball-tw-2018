<?php
require_once("apicall.php");
require_once("undefinedapicall.php");
require_once("loginapicall.php");
require_once("signupapicall.php");
require_once("gettournamentapicall.php");
require_once("logoutapicall.php");
require_once("profiledataapicall.php");
require_once("tournamentpagination.php");
require_once("searchapicall.php");
require_once("teamsdataapicallget.php");
require_once("commentapicallget.php");
require_once("tournamentinfoapicall.php");
require_once("exporttournamentapicall.php");
require_once("usersgetapicall.php");
require_once("orderedgroupapicall.php");
require_once("lastteamsapicall.php");

class ApiCallFactoryGet
{
    private $apiCall;
    
    function __construct($apiCallName)
    {     
        switch($apiCallName)
        {
            case "tournament.get":
                $this->apiCall = new GetTournamentApiCall;
            break;

            case "user.logout":
                $this->apiCall = new LogoutApiCall;
            break;

            case "user.data":
                $this->apiCall = new ProfileDataApiCall;
            break;

            case "tournament.pagination":
                $this->apiCall = new TournamentPagination;
            break;

            case "tournament.search":
                $this->apiCall = new SearchTournament;
            break;

            case "teams.data":
                $this->apiCall = new GetTeamsDataApiCall;
            break;

            case "comment.get":
                $this->apiCall = new CommentApiCallGet;
            break;
            
            case "tournament.info":
                $this->apiCall = new TournamentInfoApiCall;
            break;

            case "tournament.export":
                $this->apiCall = new ExportApiCall;
            break;

            case "users.get":
                $this->apiCall = new UsersGetApiCall;
            break;

            case "ordered.tournament":
                $this->apiCall = new OrderedGroupApiCall;
            break;

            case "last.teams":
                $this->apiCall = new LastTeamsApiCall;
            break;

            default:
                $this->apiCall = new UndefinedApiCall;
            break;
        }
    }

    function apiCall()
    {
        return $this->apiCall;
    }
}
?>