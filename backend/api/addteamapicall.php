<?php

require_once("apicall.php");

class AddTeamApiCall extends ApiCall
{
    private $error = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);

        $query = "INSERT INTO team(name, sport) VALUES(:name, :sport)";
        $assocArray = [ 'name' => $json_map["name"], 'sport' => $json_map["sport"] ];

        if( DatabaseConnection::instance()->insert($query, $assocArray) === false )
        {
            $this->error = "Could not insert team into database";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}


?>