<?php

require_once("apicall.php");

class UpdateScoresApiCall extends ApiCall
{
    private $error = "";

    function updateScores($json_map)
    {
        for($i = 0; $i < count($json_map["scores"]); ++$i)
        {
            $obj = $json_map["scores"][$i];
            $query = "UPDATE event SET result=:result WHERE eventID=:eventID";
            $assocArray = ["result" => $obj["result"], "eventID" => $obj["eventID"]];
            DatabaseConnection::instance()->insert($query, $assocArray);              
            
            $this->updatePointsInGroup($obj["eventID"], $obj["result"]);
        }
    }

    function updatePointsInGroup($eventID, $resultString) 
    {
        $query = "SELECT * FROM event WHERE eventID=:eventID";
        $assocArray = ["eventID" => $eventID];
        $result = DatabaseConnection::instance()->query($query, $assocArray);   
        
        $score = explode ("-", $resultString);

        if(intval($score[0]) > intval($score[1]))
        {
            $query = "UPDATE tournament_entry SET team_points = team_points+3 WHERE team_id=:teamID AND tournament_id=:tid";
            $assocArray = ["teamID" => $result[0]->team1ID, "tid" => $result[0]->tournamentID];
            DatabaseConnection::instance()->insert($query, $assocArray);
        }
        else if (intval($score[0]) < intval($score[1]))
        {
            $query = "UPDATE tournament_entry SET team_points = team_points+3 WHERE team_id=:teamID AND tournament_id=:tid";
            $assocArray = ["teamID" => $result[0]->team2ID, "tid" => $result[0]->tournamentID];
            DatabaseConnection::instance()->insert($query, $assocArray);            
        }
        else 
        {
            $query = "UPDATE tournament_entry SET team_points = team_points+1 WHERE team_id=:teamID AND tournament_id=:tid";
            $assocArray = ["teamID" => $result[0]->team2ID, "tid" => $result[0]->tournamentID];
            DatabaseConnection::instance()->insert($query, $assocArray);      
            $assocArray = ["teamID" => $result[0]->team1ID, "tid" => $result[0]->tournamentID];
            DatabaseConnection::instance()->insert($query, $assocArray);      
        }
    }


    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);
        $this->updateScores($json_map);
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}
?>