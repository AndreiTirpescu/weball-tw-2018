<?php

require_once("requesthandle.php");
require_once("apicall.php");
require_once("apicallfactorypost.php");

class PostRequestHandle extends RequestHandle
{
    protected function handleReceive()
    {
        $apiCallStr = $_GET['url'];
        $apiCallStr = str_replace("api/rest/", "", $apiCallStr);

        $apiCallFactory = new ApiCallFactoryPost($apiCallStr);
        $this->apiCall  = $apiCallFactory->apiCall();
        echo var_dump($this->apiCall);
        $this->apiCall->call();
    }

    protected function sendResponse()
    {
        header('Content-Type: "application/json"');
        header("Access-Control-Allow-Origin: *");

        $this->apiCall->response();
    }
}

?>