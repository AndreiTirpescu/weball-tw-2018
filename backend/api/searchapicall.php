<?php

require_once("apicall.php");

class SearchTournament extends ApiCall
{
    private $error = "";

    private $json_result = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        echo var_dump( $_GET );

        echo $_GET["input"].PHP_EOL;

        $query = 'SELECT * FROM `tournament` WHERE name like "%":input"%" OR sport like "%":input"%"';

        $assocArray = [ 'input' => $_GET["input"]];

        $result = DatabaseConnection::instance()->query($query, $assocArray);
        
        if(count($result) > 0)
        {
            $this->json_result = json_encode($result);
            echo var_dump($this->json_result);
        }
        else
        {
            $this->error = "Tournament/Sport doesn't exist in database";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            // $arr['new_user'] = '';
            $arr['data'] = $this->json_result;
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        
        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}

?>