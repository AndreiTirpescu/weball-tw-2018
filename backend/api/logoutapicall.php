<?php
require_once("apicall.php");

class LogoutApiCall extends ApiCall
{
    private $error = "";
    private $token = "";

    public function call()
    {
        echo var_dump(getallheaders());

        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $query = 'DELETE FROM token WHERE token = :token';

        $assocArray = [ 'token' => $_GET['token'] ];
              
        if(DatabaseConnection::instance()->insert($query, $assocArray) === false)
        {
            $this->error = "Could not remove token";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            $arr['token'] = "Token removed";
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}
?>