<?php

require_once("apicall.php");

class AddTournamentApiCall extends ApiCall
{
    private $error = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        if(!$this->isAdmin())
        {
            $this->error = "User access denied";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);

        $query = 'INSERT INTO tournament(name, sport, startDate, endDate, numberofteams, teams_left) VALUES(:name, :sport, :startDate, :endDate, :numberofteams, :numberofteams)';

        $assocArray = [ 'name' => $json_map['name'], 'sport' => $json_map['sport'], 'startDate' => $json_map['startDate'], 'endDate' => $json_map['endDate'], 'numberofteams' => $json_map['numberofteams'] ];
        if( DatabaseConnection::instance()->insert($query, $assocArray) === false )
        {
            $this->error = "Tournament already exists in database";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            $arr['tournament_id'] = DatabaseConnection::instance()->lastID("tournament", "tournamentID");
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        
        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}

?>