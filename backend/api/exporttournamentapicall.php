<?php
require_once("apicall.php");

class ExportApiCall extends ApiCall
{
    private $error = "";
    private $callback;
    private $tournament_entry;
    private $tournament_info;
    private $tournament_events;

    private function exportJSON()
    {
        $fh = new FileHandler();
        $fh->createFile("tournament.json");
        
        $arr = [];
        $arr['entries'] = json_encode($this->tournament_entry);
        $arr['tournament'] =  json_encode($this->tournament_info);
        $arr['events'] = json_encode($this->tournament_events);

        $fh->writeToFile(json_encode($arr));

        $fh->send("application/octet-stream");
    }

    private function exportCSV()
    {
        $fh = new FileHandler();
        $fh->createFile("tournament.csv");
        
        $data = "name;number of teams;teams left;\n";
        $data = $data.$this->tournament_info[0]->name.";".$this->tournament_info[0]->numberOfTeams.";".$this->tournament_info[0]->teams_left.";\n";

        $data = $data."name;sport;\n";
        for($i = 0; $i < count($this->tournament_entry); ++$i)
        {
            $data = $data.$this->tournament_entry[$i]->name.";".$this->tournament_entry[$i]->sport.";\n";
        }

        $data = $data."team1;team2;result\n";
        for($i = 0; $i < count($this->tournament_events); ++$i)
        {
            $data = $data.$this->tournament_events[$i]->team1ID.";".$this->tournament_events[$i]->team2ID.";".$this->tournament_events[$i]->result.";\n";
        }   

        $fh->writeToFile($data);
        $fh->send("application/octet-stream");      
    }

    public function call()
    {
        if(!$this -> isValidToken())
        {
            $this->error = "Invalid token";
        }

        $query = 'SELECT * FROM tournament t1 join tournament_entry t2 on t1.tournamentID = t2.tournament_id JOIN team t3 on t3.teamID = t2.team_id WHERE t1.tournamentID = :id';
        $assocArray = ['id' => $_GET['id']];
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        $this->tournament_entry = $result;

        $query = 'SELECT * FROM tournament WHERE tournamentID = :id';
        $assocArray = ['id' => $_GET['id']];
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        $this->tournament_info = $result;

        
        $query = 'SELECT * FROM event WHERE tournamentID = :id';
        $assocArray = ['id' => $_GET['id']];
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        $this->tournament_events = $result;
    }

    
    public function response()
    {
        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }

        if($_GET["type"] === "JSON")
        {
           $this->exportJSON();
        }
        else if ($_GET["type"] === "CSV")
        {
            $this->exportCSV();
        }
    }
}
?>