<?php

require_once("apicall.php");

class LastTeamsApiCall extends ApiCall
{
    private $error = "";

    private $json_result = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        echo var_dump( $_GET );

        if( !is_numeric($_GET["count"])  )
        {
            $this->error = "Nice try".$_GET["count"];
            return;
        }

        $query = "UPDATE tournament SET teams_left=teams_left/2 WHERE tournamentID = :id";
        DatabaseConnection::instance()->insert($query, [ 'id' => $_GET["id"]]);

        $query = "SELECT teams_left FROM tournament WHERE tournamentID = :id";
        $assocArray = [ 'id' => $_GET["id"]];
        $result2 = DatabaseConnection::instance()->query($query, $assocArray);

        var_dump($result2);

        $query = 'SELECT * FROM tournament_entry WHERE tournament_id = :id ORDER BY team_points DESC LIMIT '.$result2[0]->teams_left;
        $assocArray = [ 'id' => $_GET["id"]];
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        
        if(count($result) > 0)
        {
            $this->json_result = json_encode($result);
            echo var_dump($this->json_result);
        }
        else
        {
            $this->error = "Tournament doesn't exist in database";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            // $arr['new_user'] = '';
            $arr['data'] = $this->json_result;
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        
        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}

?>