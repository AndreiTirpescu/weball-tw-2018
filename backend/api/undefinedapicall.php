<?php
require_once("apicall.php");

class UndefinedApiCall extends ApiCall
{
    public function call()
    {}

    public function response()
    {
        $arr = array('error' => '400 Bad Request');
        echo json_encode($arr);
    }
}
?>