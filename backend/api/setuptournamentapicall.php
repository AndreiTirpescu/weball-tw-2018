<?php

require_once("apicall.php");

class TournamentSetupApiCall extends ApiCall
{
    private $error = "";

    function addGroups($json_map)
    {
        for($i = 0; $i < count($json_map["groups"]); ++$i)
        {
            $obj = $json_map["groups"][$i];

            for($j = 1; $j <= 4; ++$j)
            {
                $query = "INSERT INTO tournament_group_team(team_id, tournament_id, group_num) VALUES(:teamid, :tid, :gnum)";
                $assocArray = ["tid" => $json_map["tid"], "teamid" => $obj["team".$j], "gnum" => $i ];
                echo $query;
                DatabaseConnection::instance()->insert($query, $assocArray);
            }
        }
    }

    function addGames($json_map)
    {
        for($i = 0; $i < count($json_map["games"]); ++$i)
        {
            $obj = $json_map["games"][$i];
            $query = "INSERT INTO event(tournamentID, team1ID, team2ID) VALUES(:tid, :team1ID, :team2ID)";
            $assocArray = ["tid" => $json_map["tid"], "team1ID" => $obj["team1"], "team2ID" => $obj["team2"] ];
            echo $query;
            DatabaseConnection::instance()->insert($query, $assocArray);               
        }
    }

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);
        if($json_map["groups"] !== "-")
        {
            $this->addGroups($json_map);
        }

        $this->addGames($json_map);
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}
?>