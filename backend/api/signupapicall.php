<?php

require_once("apicall.php");

class SignUpApiCall extends ApiCall
{
    private $error = "";

    public function call()
    {
        $json_map = json_decode(file_get_contents("php://input"), true);

        $query = 'INSERT INTO user(name, email, password) VALUES(:username, :email, :password)' ;
        $assocArray = [ 'username' => $json_map['username'], 'email' => $json_map['email'], 'password' => $json_map['password'] ];
        if( DatabaseConnection::instance()->insert($query, $assocArray) === false )
        {
            $this->error = "User already exists in database";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            // $arr['new_user'] = '';
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        
        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}

?>