<?php

require_once("apicall.php");

class UpdateUserApiCall extends ApiCall
{
    private $error = "";

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $json_map = json_decode(file_get_contents("php://input"), true);

        $dbUpdateableValues = "";
        
        $assocArray = array();

        if(isset($json_map["username"]))
        {
            $dbUpdateableValues = $dbUpdateableValues."name = :username, ";
            $assocArray["username"] = $json_map["username"];
        }

        if(isset($json_map["email"]))
        {
            $dbUpdateableValues = $dbUpdateableValues."email = :email, ";
            $assocArray["email"] = $json_map["email"];
        }

        if(isset($json_map["password"]))
        {
            $dbUpdateableValues = $dbUpdateableValues."password = :password ";
            $assocArray["password"] = $json_map["password"];
        }

        $query = 'UPDATE user SET '. $dbUpdateableValues .'WHERE userID = '. $this->id_for_token;
        echo $query;
        if( DatabaseConnection::instance()->insert($query, $assocArray) === false )
        {
            $this->error = "User already exists in database";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            // $arr['new_user'] = '';
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}


?>