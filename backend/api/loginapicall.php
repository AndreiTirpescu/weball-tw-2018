<?php
require_once("apicall.php");

class LoginApiCall extends ApiCall
{
    private $error = "";
    private $token = "";
    private $isAdmin = "";

    public function call()
    {
        $json_map = json_decode(file_get_contents("php://input"), true);

        $query = 'SELECT * FROM user WHERE email = :email AND password = :password';

        $assocArray = [ 'email' => $json_map['email'], 'password' => $json_map['password']];

        $result = (DatabaseConnection::instance()->query($query, $assocArray));
 
        if(count($result) > 0)
        {
            $this->isAdmin = $result[0]->isAdmin;

            $query = 'INSERT INTO token (userID, token) VALUES(:userID, :token)';

            $time = time();
            $hashString = sha1($result[0]->userID.$result[0]->email.$time);

            $assocArray = [ 'userID' => $result[0]->userID, 'token' => $hashString];

            if(DatabaseConnection::instance()->insert($query, $assocArray) === false)
            {
                $this->error = "Couldn't insert into token table";
            } 
            else
            {
                $this->token = $hashString;
            }
        }
        else
        {
            $this->error = "Wrong username or password";
        }
    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            $arr['token'] = $this->token;
            $arr['isAdmin'] =  $this->isAdmin;
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}
?>