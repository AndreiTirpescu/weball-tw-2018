<?php

require_once("config/restconfig.php");
require_once("config/database.php");
require_once("config/filehandler.php");

abstract class ApiCall
{
    protected $id_for_token;
    protected $is_admin;

    abstract public function call();
    abstract public function response();

    function isValidToken() 
    {
        $query = "SELECT * from token WHERE token = :token";
        
        if(!isset($_GET["token"]))
        {
            return false;
        }

        $assocArray = ['token' => $_GET["token"]];
        
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        
        if(count($result) != 1) 
        {
            return false;
        }
        
        $this->id_for_token = $result[0]->userID;

        return true;
    }

    function isAdmin() 
    {
        $query = "SELECT * from user WHERE userID = :id";

        $assocArray = ['id' => $this->id_for_token];
        
        $result = DatabaseConnection::instance()->query($query, $assocArray);
        
        if(count($result) != 1) 
        {
            return false;
        }
        
        $this->is_admin = $result[0]->isAdmin;

        return $this->is_admin;
    }

}

?>
