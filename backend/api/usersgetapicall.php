<?php
require_once("apicall.php");

class UsersGetApiCall extends ApiCall
{
    private $error = "";
    private $resp = array();

    public function call()
    {
        if(!$this->isValidToken())
        {
            $this->error = "Invalid token";
            return;
        }

        $query = 'SELECT name, userID, email FROM user WHERE isAdmin = 0';
              
        $result = DatabaseConnection::instance()->query($query, $assocArray);

        if(count($result) <= 0)
        {
            $this->error = "Users table doesn't have non-admins";
            return;
        }
        
        $this->resp = json_encode($result);
        

    }

    public function response()
    {
        $arr = [];
        if($this->error === "")
        {
            $arr['status'] = 'success';
            $arr['data'] = $this->resp;
        }
        else 
        {
            $arr['status'] = 'failure';
            $arr['error'] = $this->error;
        }

        if(RestConfig::instance()->shouldRemoveDebugMessageBeforeSend())
        {
            ob_end_clean();
        }
        echo json_encode($arr);
    }
}
?>