var url = window.location.href;
var surl = url.split('?')[1];

document.getElementById("comment-form").addEventListener("submit", function(event){
    event.preventDefault();
    addcomment();
});

function addcomment(event) 
{
    // event.preventDefault();
    var comment = document.forms["comment-form"].getElementsByTagName("textarea");

    var info = {
        commentString: comment[0].value,
        tournamentID: surl
    };

    var headers = {
        "Content-type": "application/x-www-form-urlencoded"
    };

    var type = "POST";
    var url  = "http://127.0.0.1:27015/api/rest/user.comment?token=";

    url += cookieHandler.getCookie('token');

    console.log(JSON.stringify(info));

    requestHandler.buildRequest(type, JSON.stringify(info), headers, url, handleResponseAddComment);
}

function handleResponseAddComment(data)     
{
    var dataMap = JSON.parse(data);
    if(dataMap["status"] === "success")
    {
        window.location.href = url;
    }
}