var administrationModule = (function(){

    function hideFunctionalitiesIfNotAdmin()
    {
        if(cookieHandler.getCookie("09e05eafd740fc13116c962d1a6872e0412d1ecd") === "0")
        {
            var list = document.getElementsByClassName("adminLink");
            for(var i in list)
            {
                try
                {
                    list[i].style.display = "none";
                }
                catch(e)
                {
                    console.log(e);
                }    
            }
        }
    }

    function redirect()
    {
        if(cookieHandler.getCookie("09e05eafd740fc13116c962d1a6872e0412d1ecd") === "0")
        {
            window.location.href = "./inside.html";
        }
    }

    return {
        hideFunctionalitiesIfNotAdmin,
        redirect
    }

})();