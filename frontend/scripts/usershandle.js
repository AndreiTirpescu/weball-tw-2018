var usersHandler = (function() {
    
    function requestUsersInfo(callback)
    {
        var type = "GET";
        var url = "http://127.0.0.1:27015/api/rest/users.get?token=" + cookieHandler.getCookie("token");
        requestHandler.buildRequest(type, "", "", url,  callback);
    }

    function parseData(data)
    {
        
        var users = JSON.parse(JSON.parse(data)["data"]);

        return {
            users: users
        }
    }

    return {
        requestUsersInfo,
        parseData
    }

})();