document.getElementById("clickable-burger-open").onclick = function() 
{
    document.getElementsByTagName("header")[0].classList = "menu-shown";
    document.getElementById("clickable-burger-open").style.display = 'none';
    document.getElementById("clickable-burger-close").style.display = 'block';
}

document.getElementById("clickable-burger-close").onclick = function() 
{
    document.getElementsByTagName("header")[0].classList = "menu-hidden";
    document.getElementById("clickable-burger-open").style.display = 'block';
    document.getElementById("clickable-burger-close").style.display = 'none';
}

document.body.onresize = function() 
{
    if(document.body.clientWidth > 750)
    {
        document.getElementsByTagName("header")[0].classList = "";
        document.getElementById("clickable-burger-open").style.display = 'block';
        document.getElementById("clickable-burger-close").style.display = 'none';
    }
}

window.onload = function() {
    if(cookieHandler.getCookie("token") !== "")
    {
        window.location.href = "inside.html";
        return;
    }
}
    