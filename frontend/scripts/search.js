document.getElementsByClassName('search-bar')[1].onchange = function(){

    cookieHandler.writeCookie("lastTournamentID", 0);
    document.getElementsByClassName('tournament-info-section')[0].innerText = "";

    var type = "GET";
    var url = "http://127.0.0.1:27015/api/rest/tournament.search?token=" + cookieHandler.getCookie("token") + '&input=' + document.getElementsByClassName('search-bar')[1].value;

    requestHandler.buildRequest(type, "", "", url, handlePagination);

    handlePagination();
}