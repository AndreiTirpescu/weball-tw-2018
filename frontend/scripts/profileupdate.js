document.getElementById("update-user-form").addEventListener("submit", function(event){
    event.preventDefault();
    updateUserData();
});

function updateUserData(event) 
{
    // event.preventDefault();
    var form = document.forms["update-user-form"].getElementsByTagName("input");

    var info = {
        username:form[0].value,
        email:form[1].value,
        password:form[2].value
    };

    var headers = {
        "Content-type": "application/x-www-form-urlencoded"
    };

    var type = "POST";
    var url  = "http://127.0.0.1:27015/api/rest/user.update?token=";
    url += cookieHandler.getCookie('token');


    requestHandler.buildRequest(type, JSON.stringify(info), headers, url, handleResponseUpdateUserData);
}

function handleResponseUpdateUserData(data) 
{
    var dataMap = JSON.parse(data);
    if(dataMap["status"] === "success")
    {
        window.location.href = "profile.html";
    }
}