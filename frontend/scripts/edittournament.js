var teamNameMapping = [];
var teamElementMapping = [];

function requestTournamentInfo() 
{
    var url = window.location.href;
    var id = url.split('=')[1]; 

    tournamentHandler.requestTournamentInfo(id, function(data){
        var tourney = tournamentHandler.splitData(data); 
        teamNameMapping = tournamentHandler.extractTeamNameMapping(tourney);
        displayEvents(tourney)
    })
}

function displayEvents(tourney)
{
    var list = document.getElementsByClassName("tournament-events")[0];

    var k = 0;
    for(var i in tourney.events)
    {
        if(tourney.events[i]["result"] !== "")
        {
            console.info(tourney.events[i]["result"]);
            continue;
        }

        div = document.createElement('div');
        div.className = "score-updateable";

        team1 =  document.createElement('p');
        team2 =  document.createElement('p');
        input =  document.createElement('input');

        team1.innerText = teamNameMapping[tourney.events[i]["team1ID"]];
        team2.innerText = teamNameMapping[tourney.events[i]["team2ID"]];

        teamElementMapping[k++]   = tourney.events[i]["eventID"];
        
        div.appendChild(team1);
        div.appendChild(team2);
        div.appendChild(input);
        
        list.appendChild(div);
    }

    var updateButton = document.createElement("h1");
    updateButton.innerText = "update scores";
    updateButton.className = 'update-tourney';
    updateButton.onclick = updateScores;
    list.appendChild(updateButton);
}

function updateScores() {

    var list = document.getElementsByClassName("score-updateable");

    
    if(document.getElementsByClassName("score-updateable").length === 0)
    {
        requestTopCount(0);
        return;
    }

    var obj = {
        scores: []
    };

    for(var i = 0; i < list.length; ++i)
    {
        var inputField = list[i].getElementsByTagName("input")[0];
        if(inputField.value)
        {   
            obj.scores.push({ eventID: teamElementMapping[i], result: inputField.value })
        }
    }

    tournamentHandler.updateTournamentScores(obj, function(data){
        var uri = window.location.href;
        window.location.href = uri;
    })
}


function requestTopCount(count)
{
    var url = window.location.href;
    var id = url.split('=')[1]; 
    console.log(count)
    tournamentHandler.requestLastTeams(id, count, function(data){
        console.log(data);
        var lastTeams = JSON.parse(JSON.parse(data)["data"]);
        var matchArray = [];
        for(var i = 0; i < lastTeams.length - 1; i += 2)
        {
            matchArray.push({ team1: lastTeams[i]["team_id"], team2: lastTeams[i+1]["team_id"] });
        }

        var obj = 
        {
            tid: id,
            groups: "-",
            games: matchArray
        }

        console.log(JSON.stringify(obj));

        tournamentHandler.sendTournamentSetup(obj, function(data){
            if(JSON.parse(data)["status"] === "success")
            {
                var url = window.location.href;
                window.location.href = url;
            }
        });

    });
}