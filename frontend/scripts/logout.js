function logout()
{
    var type = "GET";
    var url = "http://127.0.0.1:27015/api/rest/user.logout?token=" + cookieHandler.getCookie("token");
    requestHandler.buildRequest(type, "", "", url, handleResponseLogout);
}

function handleResponseLogout() {
    cookieHandler.removeCookie("token");
    window.location.href = "index.html";
}