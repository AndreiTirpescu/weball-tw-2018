document.getElementById("signup-form").addEventListener("submit", function(event){
    event.preventDefault();
    signup();
});

function signup(event) 
{
    // event.preventDefault();
    var form = document.forms["signup-form"].getElementsByTagName("input");

    var info = {
        username: form[0].value,
        email: form[1].value,
        password: form[2].value
    };
    
    var headers = {
        "Content-type": "application/x-www-form-urlencoded"
    };

    var type = "POST";
    var url  = "http://127.0.0.1:27015/api/rest/user.signup";

    if(cookieHandler.getCookie("token") !== "")
    {
        alert("already logged in");
        return;
    }

    requestHandler.buildRequest(type, JSON.stringify(info), headers, url, handleResponseSignup);
}

function handleResponseSignup(data) 
{
    var dataMap = JSON.parse(data);
    if(dataMap["status"] === "success")
    {
        window.location.href = "login.html";
    }
}