var selector = new ClickLoader("select-team-entry", selectTeam);
selector.load();

var sentInfo;
var lastID;

var sendDiv = document.createElement('div');

var data_team_map = [];

var lastIndex;
var lastMax;

var eventNumber = 0;
var eventColor = getRandomColor();

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

document.getElementById("addtournament-form").addEventListener("submit", function(event){
    event.preventDefault();
    addtournament();
});

function addtournament(event) 
{
    var form = document.forms["addtournament-form"].getElementsByTagName("input");

    var formSelect = document.forms["addtournament-form"].getElementsByTagName("select");

    var info = {
        name: form[0].value,
        startDate: form[1].value + 'T' + form[2].value,
        sport: formSelect[0].value,
        endDate: form[3].value + 'T' + form[4].value,
        numberofteams: form[5].value
    };

    sentInfo = info;

    var headers = {
        "Content-type": "application/x-www-form-urlencoded"
    };

    var type = "POST";
    var url  = "http://127.0.0.1:27015/api/rest/tournament.add?token=";

    url += cookieHandler.getCookie('token');

    console.log(JSON.stringify(info));

    requestHandler.buildRequest(type, JSON.stringify(info), headers, url, handleResponseAddTournament);
}

function handleResponseAddTournament(data) 
{
    var dataMap = JSON.parse(data);
    if(dataMap["status"] === "success")
    {
        lastID = dataMap["tournament_id"][0]["tournamentID"];
        document.getElementById("addtournament-form").style.display = 'none';
        requestTeamsForSport();
    }
}

function requestTeamsForSport() 
{
    document.getElementsByClassName("teams-selector")[0].className = "teams-selector";
    
    var type = "GET";
    var url  = "http://127.0.0.1:27015/api/rest/teams.data?token=";
    url += cookieHandler.getCookie('token');
    url += "&sport=";
    url += sentInfo.sport;
    requestHandler.buildRequest(type, "", "", url, handleResponseGetTeamsForSport);
}

function handleResponseGetTeamsForSport(data)
{
    var dataMap = JSON.parse(data);
    if(dataMap["status"] === "success")
    {
        document.getElementById("addtournament-form").style.display = 'none';

        var dataArray = JSON.parse(dataMap["data"]);
        var selectorDiv = document.getElementsByClassName('teams-selector')[0];
        for(var i = 0; i < dataArray.length; ++i)
        {
            selectorDiv.appendChild(createDivFromTeamEntry(dataArray[i]["teamID"], dataArray[i]["name"]));
        }
        
        sendDiv.className = "add-button-teams";
        sendDiv.onclick = registerTeams;

        var addteamsh1 = document.createElement('h1');
        addteamsh1.innerText = "ADD TEAMS";

        sendDiv.appendChild(addteamsh1);

        selectorDiv.appendChild(sendDiv);
        selector.load();
    }
}

function createDivFromTeamEntry(teamID, teamName) {
    var div = document.createElement('div');
    var name  = document.createElement('h1');
    var id = document.createElement('h2');

    div.className = "select-team-entry";
    name.className = "team-name";
    id.className = "team-id";

    id.innerText = teamID;
    name.innerText = teamName;

    div.appendChild(name);
    div.appendChild(id);
    
    return div;
}


function selectTeam() 
{
    this.className += " selected-team-entry";
    this.onclick = deselectTeam;
}

function deselectTeam() 
{
    var str = this.className.replace(" selected-team-entry", "");
    this.className = str;
    this.onclick = selectTeam;
}

function registerTeams() 
{
    var selected = document.getElementsByClassName("selected-team-entry");
    if(selected.length != sentInfo.numberofteams)
    {
        alert("invalid number of teams seleected, please select or deselect, you have "  + selected.length + "/" + sentInfo.numberofteams + " selected");
        return;
    }

    console.log(selected);

    registerNext(0, selected.length);
}

function registerNext(index, max)
{
    if(index >= max)
    {
        createEvents();
        return;
    }

    lastIndex = index;
    lastMax = max;
    var selected = document.getElementsByClassName("selected-team-entry");
    var info = {
        tournamentid:lastID,
        teamid:selected[index].getElementsByClassName("team-id")[0].innerText
    };

    var headers = {
        "Content-type": "application/x-www-form-urlencoded"
    };

    var type = "POST";
    var url  = "http://127.0.0.1:27015/api/rest/tournament.entry?token=";
    url += cookieHandler.getCookie('token');
    console.log(info);

    requestHandler.buildRequest(type, JSON.stringify(info), headers, url, function(data){ 
        registerNext(lastIndex + 1, lastMax);
    });
}

function createEvents(num)
{
    window.location.href = "setuptournament.html?id=" + lastID;
}

function getTournamentEntries()
{
    var type = "GET";
    var url = "http://127.0.0.1:27015/api/rest/tournament.info?token=" + cookieHandler.getCookie("token");
    url += '&id=' + lastID;

    requestHandler.buildRequest(type, "", "", url,  function(data){
        var entries = JSON.parse(JSON.parse(data)["entries"]);
        var tournament = JSON.parse(JSON.parse(data)["tournament"]);
        // document.getElementById("tournament-name").innerText = tournament[0]["name"];
        document.getElementsByClassName("teams-selector")[0].innerText = "";

        for(var i = 0; i < entries.length; ++i)
        {
            console.log(entries[i]);
            var createdDiv = createDivFromTeamEntry(entries[i]["name"], "");
            document.getElementsByClassName("teams-selector")[0].appendChild(createdDiv);
            data_team_map[ i ] = entries[i]["tournamentID"];
        }

        for(var i = 0; i < data_team_map.length; ++i)
        {
            document.getElementsByClassName("select-team-entry")[i].onclick = setEvent;
        }
    });
}

function setEvent() {
    this.style.backgroundColor = eventColor;
    this.onclick = unsetEvent();
}

function unsetEvent() {
    // this.style.backgroundColor = "aliceblue";
    this.onclick = setEvent();
}