var exportHandler = (function() {
    
    function downloadExport(id, dwtype)
    {
        var type = "GET";
        var url = "http://127.0.0.1:27015/api/rest/tournament.export?token=" + cookieHandler.getCookie("token");
        url += '&id=' + id;
        url += "&type=" + dwtype;
        
        console.log(url);
        
        downloadFile(url);
    }

    function downloadFile(lnk)
    {
        var linkElement = document.createElement("a")
            
        linkElement.href = lnk
        linkElement.style.display = 'none'

        document.body.appendChild(linkElement)
        linkElement.click()
        document.body.removeChild(linkElement)
    }

    return {
        downloadExport
    }

})();