document.getElementById("addteam-form").addEventListener("submit", function(event){
    event.preventDefault();
    addteam();
});

function addteam(event) 
{
    // event.preventDefault();
    var form = document.forms["addteam-form"].getElementsByTagName("input");

    var formSelect = document.forms["addteam-form"].getElementsByTagName("select");


    var info = {
        name: form[0].value,
        sport: formSelect[0].value,
    };


    var headers = {
        "Content-type": "application/x-www-form-urlencoded"
    };

    var type = "POST";
    var url  = "http://127.0.0.1:27015/api/rest/team.add?token=";

    url += cookieHandler.getCookie('token');

    console.log(JSON.stringify(info));

    requestHandler.buildRequest(type, JSON.stringify(info), headers, url, handleResponseAddTeam);
}

function handleResponseAddTeam(data) 
{
    var dataMap = JSON.parse(data);
    if(dataMap["status"] === "success")
    {   
        window.location.href = "./inside.html";
    }
}