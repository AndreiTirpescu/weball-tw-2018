function printUserInfo(){

    usersHandler.requestUsersInfo(function  (data)
    {

        var usersArray = usersHandler.parseData(data).users;

          for (i in usersArray ){
        
            var div = document.createElement('div');
            var name = document.createElement('p');
            
            name.innerText = usersArray[i]["email"];

            name.className = "user-info-nonAdmin unselected-user-entry";
            
            name.onclick = selectUser;
            div.appendChild(name);

            document.getElementsByClassName('users-info')[0].appendChild(div);

        }

        var par = document.createElement('p');
        par.innerText = "Make Admin";
        document.getElementsByClassName('users-info')[0].appendChild(par);

        par.onclick = makeAdmin;

    });

}

function makeAdmin()
{
    var list = document.getElementsByClassName('selected-user-entry');
    var array = [];
    for (var i = 0; i<list.length; ++i){
        array[i] = list[i].innerText;

    }
    console.log(JSON.stringify(array));

    var headers = {
        "Content-type": "application/x-www-form-urlencoded"
    };

    var type = "POST";
    var url  = "http://127.0.0.1:27015/api/rest/user.upgrade?token=";

    url += cookieHandler.getCookie('token');

    var obj = {"email" : array};

    requestHandler.buildRequest(type, JSON.stringify(obj), headers, url, handleResponseUserUpgrade);
}

function selectUser()
{
    var str = this.className.replace("unselected-user-entry", "selected-user-entry");
    this.className = str;
    this.onclick = deselectUser;
}

function deselectUser()
{
    var str = this.className.replace("selected-user-entry", "unselected-user-entry");
    this.className = str;
    this.onclick = selectUser;
}


function handleResponseUserUpgrade(data) 
{
    var dataMap = JSON.parse(data);
    if(dataMap["status"] === "success")
    {   
        window.location.href;
    }
}