var swapCount = 0;
var team_ids = [];
var has_groups = false;

var url = window.location.href;
var id = url.split('=')[1];

function setupTournament()
{
    tournamentHandler.requestTournamentInfo(id, function(data){
        var tourney = tournamentHandler.splitData(data);
        document.getElementById("tournament-name").innerText = tourney.info["name"]
        console.log(parseInt(tourney.info["numberOfTeams"]) );
        if(parseInt(tourney.info["numberOfTeams"]) < 32)
        {
            setupTournamentWithoutGroups(tourney, 2);
        }
        else
        {
            has_groups = true;
            setupTournamentWithoutGroups(tourney, 4);
        }
    })
}

function setupTournamentWithoutGroups(tourney, count)
{
    console.log(tourney.teams)

    var listElement = document.getElementsByClassName("team-groups")[0];
    var groups = document.getElementById(groups)
    
    for(var i = 0; i <  tourney.teams.length; i += count)
    {
        var matchDiv = document.createElement("div");
        matchDiv.className += "match-div";

        for(var j = 0; j < count; ++j) 
        {
            team_ids[i+j] = tourney.teams[i+j]["teamID"]
            var p = document.createElement("p")
            p.innerText = tourney.teams[i + j]["name"]
            p.className = "normal-match-entry";
            matchDiv.appendChild(p)
        }

        listElement.appendChild(matchDiv);
    }

    var generateButton = document.createElement("h1");
    generateButton.innerText = "generate tournament";
    generateButton.className = "generate-button";
    listElement.appendChild(generateButton);

    var entries = document.getElementsByClassName("normal-match-entry");
    for(var i = 0; i < entries.length; ++i)
    {
        entries[i].onclick = matchSwap;
    }
    console.log(has_groups);
    if(has_groups == true)
    {
        document.getElementsByClassName("generate-button")[0].onclick = generateTourneyWithGroups;
    }
    else
    {
        document.getElementsByClassName("generate-button")[0].onclick = generateTourney;
    }
}

function matchSwap()
{
    if(this.className.indexOf("swap") !== -1)
    {
        var newClassName = this.className.replace("swap", "normal-match-entry");
        this.className = newClassName;
        --swapCount;
        return;
    }

    var newClassName = this.className.replace("normal-match-entry", "swap");
    this.className = newClassName;
    ++swapCount;

    if(swapCount == 2)
    {
        swapEntries();
        return;
    }
}

function getIndexOfEntry(entry)
{
    var list = document.getElementsByClassName("team-groups")[0].getElementsByTagName("p");
    for(var i = 0; i < list.length; ++i)
    {
        if(entry === list[i])
        {
            return i;
        }
    }
    return -1;
}

function swapEntries()
{
    var entry1 = document.getElementsByClassName("swap")[0];
    var entry2 = document.getElementsByClassName("swap")[1];
    
    var aux = entry1.innerText;
    entry1.innerText = entry2.innerText;
    entry2.innerText = aux;
    
    var newClassName = entry1.className.replace("swap", "normal-match-entry");
    entry1.className = newClassName;
    entry2.className = newClassName;

    swapIndexes(getIndexOfEntry(entry1), getIndexOfEntry(entry2));

    swapCount = 0;
}

function swapIndexes(idx1, idx2)
{
    var tmp = team_ids[idx1];
    team_ids[idx1] = team_ids[idx2];
    team_ids[idx2] = tmp;
}

function generateTourney()
{
    var matchArray = [];
    for(var i = 0; i < team_ids.length; i += 2)
    {
        matchArray.push({ team1: team_ids[i], team2: team_ids[i+1] });
    }
    
    var obj = 
    {
        tid: id,
        groups: "-",
        games: matchArray
    }

    console.log(JSON.stringify(obj));

    tournamentHandler.sendTournamentSetup(obj, function(data){
        if(JSON.parse(data)["status"] === "success")
        {
            window.location.href = "tournament.html?id=" + id;
        }
    });
}


function generateTourneyWithGroups()
{
    var groups = [];
    var matchArray = [];
    console.log(team_ids.length)
    for(var i = 0; i < team_ids.length; i += 4)
    {
        groups.push({ team1: team_ids[i], team2: team_ids[i+1],
                      team3: team_ids[i+2], team4: team_ids[i+3] });

        matchArray.push({ team1: team_ids[i], team2: team_ids[i+1] });
        matchArray.push({ team1: team_ids[i+2], team2: team_ids[i+3] });

        matchArray.push({ team1: team_ids[i], team2: team_ids[i+2] });
        matchArray.push({ team1: team_ids[i+1], team2: team_ids[i+3] });

        matchArray.push({ team1: team_ids[i], team2: team_ids[i+3] });
        matchArray.push({ team1: team_ids[i+1], team2: team_ids[i+2] });
    }
    
    var obj = 
    {
        tid: id,
        groups: groups,
        games: matchArray
    }

    tournamentHandler.sendTournamentSetup(obj, function(data){
        if(JSON.parse(data)["status"] === "success")
        {
            window.location.href = "tournament.html?id=" + id;
        }
    });
}