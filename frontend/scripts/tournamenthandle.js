var tournamentHandler = (function() {
    
    function extractTeamNameMapping(tourney)
    {
        var teamNameMapping = [];
        for (var i in tourney.teams)
        {
            teamNameMapping[ tourney.teams[i]["team_id"] ]  = tourney.teams[i]["name"];
        }
        return teamNameMapping;
    }

    function requestTournamentInfo(id, callback)
    {
        var type = "GET";
        var url = "http://127.0.0.1:27015/api/rest/tournament.info?token=" + cookieHandler.getCookie("token");
        url += '&id=' + id;
        requestHandler.buildRequest(type, "", "", url,  callback);
    }

    function requestLastTeams(id, count, callback)
    {
        if(count < 2)
        {
            count = 1;
        }
        
        var type = "GET";
        var url = "http://127.0.0.1:27015/api/rest/last.teams?token=" + cookieHandler.getCookie("token");
        url += '&id=' + id;
        url += "&count=" + count;
        console.log(url);
        requestHandler.buildRequest(type, "", "", url,  callback);
    }

    function splitData(data)
    {
        var entries = JSON.parse(JSON.parse(data)["entries"]);
        var tournament = JSON.parse(JSON.parse(data)["tournament"]);
        var events = JSON.parse(JSON.parse(data)["events"]);
        
        return {
            teams: entries,
            info: tournament[0],
            events: events
        }
    }
    
    function sendTournamentSetup(obj, callback)
    {
        var headers = {
            "Content-type": "application/x-www-form-urlencoded"
        };
    
        var type = "POST";
        var url  = "http://127.0.0.1:27015/api/rest/tournament.setup?token=";
        url += cookieHandler.getCookie('token');
    
        requestHandler.buildRequest(type, JSON.stringify(obj), headers, url, callback);
    }

    function updateTournamentScores(obj, callback)
    {
        var headers = {
            "Content-type": "application/x-www-form-urlencoded"
        };
    
        var type = "POST";
        var url  = "http://127.0.0.1:27015/api/rest/update.scores?token=";
        url += cookieHandler.getCookie('token');
    
        requestHandler.buildRequest(type, JSON.stringify(obj), headers, url, callback);
    }

    return {
        requestTournamentInfo,
        splitData,
        sendTournamentSetup,
        updateTournamentScores,
        extractTeamNameMapping,
        requestLastTeams
    }

})();