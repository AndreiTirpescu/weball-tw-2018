function getUserData()
{
    var type = "GET";
    var url = "http://127.0.0.1:27015/api/rest/user.data?token=" + cookieHandler.getCookie("token");
    requestHandler.buildRequest(type, "", "", url, handleResponseUserData);
}

function handleResponseUserData(data) 
{
    var usr = JSON.parse(data)["data"];
    
    try
    {
        document.getElementById("profile-username").innerText = usr["username"];
        document.getElementById("profile-email").innerText = usr["email"];
        document.getElementById("profile-picture").src = usr["profilePic"];
    }
    catch(e)
    {
        console.log(e);
    }

    try 
    {
        document.getElementById("left-menu-profile-pic").src = usr["profilePic"];
    }
    catch(e)
    {
        console.log(e);
    }

}