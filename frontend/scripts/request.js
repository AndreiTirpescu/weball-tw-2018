var requestHandler = (function(){
    
    function buildRequest(type, body, headers, url, callback)
    {
        var xhr = new XMLHttpRequest();
        xhr.open(type, url, true);
        
        for(var i = 0; i < Object.keys(headers).length; ++i)
        {
            xhr.setRequestHeader(Object.keys(headers)[i], headers[Object.keys(headers)[i]]);
        }

        xhr.onreadystatechange = function() {
            if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) 
            {
                console.log(xhr.responseText);
                callback(xhr.responseText);
            }
        }

        xhr.send(body);
    }

    return {
        buildRequest: buildRequest
    }

})();
