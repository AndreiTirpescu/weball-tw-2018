window.onload = function() {
    if(cookieHandler.getCookie("token") !== "")
    {
        window.location.href = "inside.html";
        return;
    }
}


document.getElementById("login-form").addEventListener("submit", function(event){
    event.preventDefault();
    login();
});

function login(event) 
{
    // event.preventDefault();
    var form = document.forms["login-form"].getElementsByTagName("input");

    var info = {
        email: form[0].value,
        password: form[1].value
    };
    
    var headers = {
        "Content-type": "application/x-www-form-urlencoded"
    };

    var type = "POST";
    var url  = "http://127.0.0.1:27015/api/rest/user.login";

    if(cookieHandler.getCookie("token") !== "")
    {
        alert("already logged in");
        return;
    }

    requestHandler.buildRequest(type, JSON.stringify(info), headers, url, handleResponseLogin);
}

function handleResponseLogin(data) 
{
    var dataMap = JSON.parse(data);
    if(dataMap["status"] === "success")
    {
        cookieHandler.writeCookie("token", dataMap["token"]);
        cookieHandler.writeCookie("09e05eafd740fc13116c962d1a6872e0412d1ecd", dataMap["isAdmin"]);
        window.location.href = "./inside.html";
    }
}