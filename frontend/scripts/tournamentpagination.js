cookieHandler.writeCookie("lastTournamentID", 0);

window.onscroll = function(ev) {
    if ((window.innerHeight + window.pageYOffset ) >= document.body.offsetHeight) {
        paginate();
    }
};

function paginate()
{   
    var type = "GET";
    var url = "http://127.0.0.1:27015/api/rest/tournament.pagination?token=" + cookieHandler.getCookie("token");

    if(cookieHandler.getCookie("lastTournamentID") === "")
    {
        cookieHandler.writeCookie("lastTournamentID", 0);
    }

    url += "&id=" + cookieHandler.getCookie("lastTournamentID");

    requestHandler.buildRequest(type, "", "", url, handlePagination);
}

function handlePagination(data){
    
    console.log(JSON.parse(JSON.parse(data)["data"]));

    var jsonArray = JSON.parse(JSON.parse(data)["data"]);

    for(var i = 0; i < jsonArray.length; ++i)
    {

        var iDiv = document.createElement('div');

        if(i % 2 === 0)
        {
            iDiv.className = 'tournament-info black-tournament button-expand';
        }
        else
        {
            iDiv.className = 'tournament-info white-tournament button-expand';
        }
        
        var header1 = document.createElement('h1');
        var header2 = document.createElement('h1');

        header1.innerText = jsonArray[i]["name"];

        header2.innerText = jsonArray[i]["sport"];

        iDiv.appendChild(header1);
        iDiv.appendChild(header2);

        var iDivHidden = document.createElement('div');
        iDivHidden.className = 'wrapper-features-hidden';

        var iDivHiddenDiv1 = document.createElement('div');

        iDivHiddenDiv1.className = 'feature-tournament-info';

        var paragraph = document.createElement('p');
        paragraph.innerText = "Get tournament info in specific format";

        var aLink = document.createElement('a');
        aLink.className = 'view-tournament';
        aLink.href = "tournament.html?id=" + jsonArray[i]["tournamentID"];
        aLink.innerText = "View in new window";

        var paragraph1 = document.createElement('p');
        paragraph1.className = 'downloadable-info-feature json-generator';

        var paragraph2 = document.createElement('p');
        paragraph2.className = 'downloadable-info-feature xml-generator';

        var paragraph3 = document.createElement('p');
        paragraph3.className = 'downloadable-info-feature pdf-generator';

        var paragraph4 = document.createElement('p');
        paragraph4.className = 'downloadable-info-feature csv-generator';

        var iDivHiddenDiv2 = document.createElement('a');

        iDivHiddenDiv2.className = "view-tournament";

        iDivHiddenDiv2.href="updatetournament.html?id=" + jsonArray[i]["tournamentID"];

        iDivHiddenDiv2.innerText = "Modify tournament/update result";

        var iDivHiddenDiv3 = document.createElement('div');

        var iDivHiddenDiv4 = document.createElement('div');

        iDivHiddenDiv4.innerText = "Join tournament";

        paragraph1.innerText = "JSON";
        paragraph2.innerText = "XML";
        paragraph3.innerText = "PDF";
        paragraph4.innerText = "CSV";

        iDivHiddenDiv1.appendChild(paragraph);
        iDivHiddenDiv1.appendChild(aLink);
        iDivHiddenDiv1.appendChild(paragraph1);
        iDivHiddenDiv1.appendChild(paragraph2);
        iDivHiddenDiv1.appendChild(paragraph3);
        iDivHiddenDiv1.appendChild(paragraph4);

        iDivHidden.appendChild(iDivHiddenDiv1);
        iDivHidden.appendChild(iDivHiddenDiv2);
        iDivHidden.appendChild(iDivHiddenDiv3);
        iDivHidden.appendChild(iDivHiddenDiv4);

        iDiv.appendChild(iDivHidden);

        document.getElementsByClassName('tournament-info-section')[0].appendChild(iDiv);
    }

    refreshExpandable();

    cookieHandler.writeCookie("lastTournamentID", jsonArray[i-1]['tournamentID']);
}