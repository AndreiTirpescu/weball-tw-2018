var url = window.location.href;
var surl = url.split('?')[1];

getTournamentData(surl);

function getTournamentData(idd)
{
    var type = "GET";
    var url = "http://127.0.0.1:27015/api/rest/tournament.info?token=" + cookieHandler.getCookie("token");
    url += '&id=' + idd;
    requestHandler.buildRequest(type, "", "", url,  function(data){
        getUserData();
        var entries = JSON.parse(JSON.parse(data)["entries"]);
        var tournament = JSON.parse(JSON.parse(data)["tournament"]);
        document.getElementById("tournament-name").innerText = tournament[0]["name"];
        
        for(var i = 0; i < entries.length; ++i)
        {
            console.log(entries[i]);
            var createdDiv = createDivFromTeamEntry(entries[i]["name"], "");
            document.getElementsByClassName("tournament-info-big")[0].appendChild(createdDiv);
        }    
    });
}

function createDivFromTeamEntry(teamID, teamName, callback) 
{
    var div = document.createElement('div');
    var name  = document.createElement('h1');
    var id = document.createElement('h2');

    div.className = "select-team-entry";
    name.className = "team-name";
    id.className = "team-id";

    id.innerText = teamID;
    name.innerText = teamName;

    div.appendChild(name);
    div.appendChild(id);
    
    return div;
}

function displayTournamentInfo()
{
    var url = window.location.href;
    var id = url.split('=')[1]; 
    console.log("what", id);
    tournamentHandler.requestTournamentInfo(id, function(data){
        var tourney = tournamentHandler.splitData(data);
        displayParticipants(tourney);
        displayFinishedMatches(tourney);
        
    })
}

function displayParticipants(tourney)
{
    var infoDiv = document.getElementsByClassName("tournament-information")[0];
        
    var tName = document.createElement("h1");
    tName.className = "tourney-name";
    tName.innerText = tourney.info["name"];

    infoDiv.appendChild(tName);

    var infoList = document.createElement("div");
    infoList.className = "info-list-tourney";

    var part = document.createElement("h3");
    part.innerText = "Participants";

    infoList.appendChild(part);
    
    for(var i = 0; i < tourney.teams.length; ++i)
    {
        var p = document.createElement("p");
        p.innerText = tourney.teams[i]["name"];
        infoList.appendChild(p); 
    }

    infoDiv.appendChild(infoList);
}

function displayFinishedMatches(tourney)
{
    var teamNameMapping = [];
    teamNameMapping = tournamentHandler.extractTeamNameMapping(tourney);

    for(var i in tourney.events)
    {
        if(tourney.events[i]["result"] === "")
        {
            console.info(tourney.events[i]["result"]);
            continue;
        }   
        div = document.createElement('div');
        div.className = 'finished-matches';

        match = document.createElement('p');

        match.innerText = teamNameMapping[tourney.events[i]["team1ID"]];
        match.innerText += " " + tourney.events[i]["result"];
        match.innerText += " " + teamNameMapping[tourney.events[i]["team2ID"]];

        div.appendChild(match);

        document.getElementsByClassName("finished-event")[0].appendChild(div);

    }
}

function displayTournamentGroups(tourney)
{
    if(tourney.info["numberOfTeams"] < 32)
        return;
    else
    {
        var type = "GET";
        var url = "http://127.0.0.1:27015/api/rest/ordered.tournament?token=" + cookieHandler.getCookie("token");
        console.log(surl);
        url += "&id" + surl;
    
        console.log(url);
    
        requestHandler.buildRequest(type, "", "", url, handleGroups);
    }
    }
}