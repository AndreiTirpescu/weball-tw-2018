var url = window.location.href;
var surl = url.split('?')[1];
surl = surl.split('=')[1];

function getComments()
{   
    var type = "GET";
    var url = "http://127.0.0.1:27015/api/rest/comment.get?token=" + cookieHandler.getCookie("token");
    url += "&tournamentID=" + surl;

    console.log(url);

    requestHandler.buildRequest(type, "", "", url, handleCommentss);
}

function handleCommentss(data){

    console.log(JSON.parse(JSON.parse(data)["data"]));

    var jsonArray = JSON.parse(JSON.parse(data)["data"]);

    for(var i = 0; i < jsonArray.length; ++i)
    {
        var iDiv = document.createElement('div');

        iDiv.className = "comment";

        var header = document.createElement('h3');
        header.innerText = jsonArray[i]["name"];

        iDiv.appendChild(header);

        var paragraph = document.createElement('p');
        paragraph.innerText = jsonArray[i]["commentString"];

        iDiv.appendChild(paragraph);

        document.getElementsByClassName('users-feedback')[0].appendChild(iDiv);
    }
}


